@extends('master')
@section('content')
<div class="mt-3 ml-3">
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Pertanyaan</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      @if(session('berhasil'))
        <div class="alert alert-success">
          {{ session('berhasil')}}
        </div>
      @endif
      <a class="btn btn-info" href="{{route('pertanyaan.create')}}">Buat Judul</a>
      <table class="table table-bordered">
        <thead>                  
          <tr>
            <th style="width: 10px">#</th>
            <th>Judul</th>
            <th>Isi</th>
            <th style="width: 40px">Action</th>
          </tr>
        </thead>
        <tbody>
          @forelse($pertanyaan as $key => $pertanyaan)
            <tr>
              <td>{{$key + 1}}</td>
              <td>{{$pertanyaan->judul}}</td>
              <td>{{$pertanyaan->isi}}</td>
              <td style="display: flex;">
                <a href="{{route('pertanyaan.show', $pertanyaan->id)}}" class="btn btn-info btn-sm">Show</a>
                <a href="{{route('pertanyaan.edit', $pertanyaan->id)}}" class="btn btn-default btn-sm">Edit</a>
                <form action="{{route('pertanyaan.destroy', $pertanyaan->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <input type="submit" value="delete" class="btn btn-danger btn-sm">
                </form>
              </td>
            </tr>
          @empty
          <p>No Judul</p>
          @endforelse
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
</div>
@endsection