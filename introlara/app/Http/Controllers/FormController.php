<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
	public function index()
    {
        return view('form');
    }

    public function create()
    {
        return view('form');
    }

    public function store()
    {
    	return redirect()->route('form.index');
    }
}


 