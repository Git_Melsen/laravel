<!DOCTYPE html>
<html>
<head>
	<title>Form | Create Account</title>
	<h1>Buat Account Baru!</h1>
</head>
<body>
	<form action="{{ route('form.store') }}" method="POST">
		<h3>Sign Up Form</h3>
		<label>First Name:</label><br />
		<input type="text" name="first_name"><br />
		<label>Last Name:</label><br />
		<input type="text" name="last_name"><br /><br />
		<label>Gender:</label><br />
		<input type="radio" name="male"/>Male<br />
		<input type="radio" name="female"/>Female<br />
		<input type="radio" name="other"/>Other<br /><br>
		<label>Nationality:</label><br>
		<select>
			<option>Indonesian</option>
			<option>Singapore</option>
			<option>Malaysian</option>
			<option>Russia</option>
		</select><br /><br>
		<label>Language Spoken:</label><br />
		<input type="checkbox">Bahasa Indonesia<br />
		<input type="checkbox">English<br />
		<input type="checkbox">Other<br /><br>
		<label>Bio:</label><br />
		<textarea name="bio" cols="40" rows="10"></textarea><br />
		<button type="submit">Sign Up</button>
	</form>
</body>
</html>